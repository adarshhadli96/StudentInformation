﻿using StudentInformation.Coderefactored.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace StudentInformation.Coderefactored.Sevices
{
    public class StudentService
    {

        #region method
      
        public void Displaystudentdetails(StudentInformation.Coderefactored.Models.Student student)
        {
            Console.WriteLine("StudentID:-> " + student.Id);
            Console.WriteLine("Firstname:-> " + student.FirstName);
            Console.WriteLine("Lastname:-> " + student.LastName);
            Console.WriteLine("Age:->" + student.Age);
            Console.WriteLine("AdmissionDate:->" + student.AdmissionDate.ToString("dddd, dd MMMM yyyy"));
            Console.WriteLine("IsUG:->" + student.IsUG);
            Console.WriteLine("IsPG:->" + student.IsPG);
            Console.WriteLine("Department:->" + student.DepartmentId);
        }
        
        public void Getstudentdetails(List<StudentInformation.Coderefactored.Models.Student> students )
        {
            foreach (StudentInformation.Coderefactored.Models.Student student in students)
            {
                Displaystudentdetails(student);
            }
        }

        public void Displaystudentfilterdetails(List<StudentInformation.Coderefactored.Models.Student> students)
        {
            students.Where(a => a.Age > 23).
                ToList().
                ForEach(a =>
                {
                    Displaystudentdetails(a);
                });

            /* students.Where(b => b.FirstName == "Sachin").
                 ToList().
                 ForEach(b => 
                 {
                     Displaystudentdetails(b);
                     });*/

        }
       #endregion method
    }
}
