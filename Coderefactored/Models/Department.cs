﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentInformation.Coderefactored.Models.Base;

namespace StudentInformation.Coderefactored.Models
{
    public class Department : BaseEntity
    {
        public Department()
        {

        }

        public string Name { get; set; }

        public static Department Create(int id, string name)
        {
            Department department = new Department
            {

                Id = id,
                Name = name,
            };
            return department;
        }
    }
}
