﻿using StudentInformation.Coderefactored.Models;



namespace StudentInformation.Coderefactored.DataSource
{


    public static class TrialData
    {
      private static List<StudentInformation.Coderefactored.Models.Student> lststudents = new List<StudentInformation.Coderefactored.Models.Student>();

       private static List<Department> lstdepartments = new List<Department>();

        

        public static List<StudentInformation.Coderefactored.Models.Student> GetStudents()
        {
            StudentInformation.Coderefactored.Models.Student studentobj1 = StudentInformation.Coderefactored.Models.Student.Create(1, "Raghu", "P", 21, new DateTime(2016, 06, 16), true, "N", 1);
            StudentInformation.Coderefactored.Models.Student studentobj2 = StudentInformation.Coderefactored.Models.Student.Create(2, "Rakesh", "R", 22, new DateTime(2017, 07, 17), true, "N", 2);
            StudentInformation.Coderefactored.Models.Student studentobj3 = StudentInformation.Coderefactored.Models.Student.Create(3, "Ravi", "S", 26, new DateTime(2018, 08, 18), true, "N", 3);
            StudentInformation.Coderefactored.Models.Student studentobj4 = StudentInformation.Coderefactored.Models.Student.Create(4, "Shankar", "D", 24, new DateTime(2019, 09, 18), true, "N", 4);
            StudentInformation.Coderefactored.Models.Student studentobj5 = StudentInformation.Coderefactored.Models.Student.Create(5, "Abhishek", "G", 23, new DateTime(2018, 08, 18), true, "N", 5);
            StudentInformation.Coderefactored.Models.Student studentobj6 = StudentInformation.Coderefactored.Models.Student.Create(6, "Arun", "K", 25, new DateTime(2018, 08, 18), true, "N", 5);
            StudentInformation.Coderefactored.Models.Student studentobj7 = StudentInformation.Coderefactored.Models.Student.Create(7, "Bharath", "J", 28, new DateTime(2018, 08, 18), true, "N", 3);
            StudentInformation.Coderefactored.Models.Student studentobj8 = StudentInformation.Coderefactored.Models.Student.Create(8, "Vijay", "L", 27, new DateTime(2018, 08, 18), true, "N", 6);
            StudentInformation.Coderefactored.Models.Student studentobj9 = StudentInformation.Coderefactored.Models.Student.Create(9, "Chetan", "V", 29, new DateTime(2018, 08, 18), true, "N", 7);
            StudentInformation.Coderefactored.Models.Student studentobj10 = StudentInformation.Coderefactored.Models.Student.Create(10, "Aditya", "B", 22, new DateTime(2018, 08, 18), true, "N", 8);
            

            lststudents.Add(studentobj1);
            lststudents.Add(studentobj2);
            lststudents.Add(studentobj3);
            lststudents.Add(studentobj4);
            lststudents.Add(studentobj5);
            lststudents.Add(studentobj6);
            lststudents.Add(studentobj7);
            lststudents.Add(studentobj8);
            lststudents.Add(studentobj9);
            lststudents.Add(studentobj10);

            return lststudents;
        }

        public static List<Department> GetDepartments()
        {
            Department departmentobj1 = Department.Create(1, "Dep1");
            Department departmentobj2 = Department.Create(2, "Dep2");
            Department departmentobj3 = Department.Create(3, "Dep3");
            Department departmentobj4 = Department.Create(4, "Dep4");
            Department departmentobj5 = Department.Create(5, "Dep5");
            Department departmentobj6 = Department.Create(6, "Dep6");
            Department departmentobj7 = Department.Create(7, "Dep7");
            Department departmentobj8 = Department.Create(8, "Dep8");
            Department departmentobj9 = Department.Create(9, "Dep9");
            Department departmentobj10 = Department.Create(10, "Dep10");

            lstdepartments.Add(departmentobj1);
            lstdepartments.Add(departmentobj2);
            lstdepartments.Add(departmentobj3);
            lstdepartments.Add(departmentobj4);
            lstdepartments.Add(departmentobj5);
            lstdepartments.Add(departmentobj6);
            lstdepartments.Add(departmentobj7);
            lstdepartments.Add(departmentobj8);
            lstdepartments.Add(departmentobj9);
            lstdepartments.Add(departmentobj10);

            return lstdepartments;
        }
    }
}
