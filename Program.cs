﻿/*using StudentInformation;


#region constructor injection

        *//*Student obj = new Student();
        obj.Displayfl();
        string val = obj.Displaystringfl();
        Console.WriteLine(val);


        Student obj1 = new Student("Bharath");
        obj1.Displayfl();
        string val1 = obj1.Displaystringfl();
        Console.WriteLine(val1);


        Student obj2 = new Student("Bharath","J");
        obj2.Displayfl();
        string val2 = obj2.Displaystringfl();
        Console.WriteLine(val2);
*//*
#endregion constuctor injection


#region Property injection

        Student obj3 = new Student();
        obj3.fname = "Adarsh";
        obj3.lname = "MH";
        obj3.Displayfl();
        string val3 = obj3.Displaystringfl();
        Console.WriteLine(val3);

        Student obj4 = new Student();
        obj4.FirstName = "Abhishek";
        obj4.LastName = "B";
        obj4.Age = 21;
        obj4.AdmissionDate = new DateTime(2006, 08, 17);
        obj4.IsUG = false;
        obj4.IsPG = "Yes";
        obj4.Displaystudentdetails(obj4);
        obj4.Studentanug(obj4);

        Student obj5 = new Student();
        obj5.FirstName = "Akhil";
        obj5.LastName = "D";
        obj5.Age = 21;
        obj5.AdmissionDate = new DateTime(2008, 08, 17);
        obj5.IsUG = true;
        obj5.IsPG = "No";
        obj5.Displaystudentdetails(obj5);
        obj5.Studentanug(obj5);


       List<Student> students = new List<Student>();

        Student obj7 = new Student();
        obj7.FirstName = "Sachin";
        obj7.LastName = "V";
        obj7.Age = 22;
        obj7.AdmissionDate = new DateTime(2008, 08, 17);
        obj7.IsUG = true;
        obj7.IsPG = "No";
        obj7.Displaystudentdetails(obj7);
        obj7.Studentanug(obj7);
        students.Add(obj7);


        Student obj6 = new Student();
        obj6.FirstName = "Vinay";
        obj6.LastName = "PM";
        obj6.Age = 25;
        obj6.AdmissionDate = new DateTime(2008, 08, 17);
        obj6.IsUG = true;
        obj6.IsPG = "No";
        obj6.Displaystudentdetails(obj6);
        obj6.Studentanug(obj6);
        students.Add(obj6);
        

        Student obj8 = new Student();
        obj8.FirstName = "Darshan";
        obj8.LastName = "T";
        obj8.Age = 23;
        obj8.AdmissionDate = new DateTime(2011, 08, 17);
        obj8.IsUG = true;
        obj8.IsPG = "No";
        obj8.Displaystudentdetails(obj8);
        obj8.Studentanug(obj8);
        students.Add(obj8);
        

        Student obj10 = new Student();
        obj10.FirstName = "Sachin";
        obj10.LastName = "P";
        obj10.Age = 20;
        obj10.AdmissionDate = new DateTime(2008, 08, 17);
        obj10.IsUG = true;
        obj10.IsPG = "No";
        obj10.Displaystudentdetails(obj10);
        obj10.Studentanug(obj10);
        students.Add(obj10);

#endregion Property injection


#region method injection


Student obj9 = new Student();
obj9.Displayfl("Ganesh", "LG");
string val9 = obj9.Displaystringfl("Ganesh", "LG");
Console.WriteLine(val9);


#endregion method injection*/

using StudentInformation.Coderefactored.Models;
using StudentInformation.Coderefactored.Sevices;


/*studentobj.FirstName = "Raghu";
studentobj.LastName = "P";
studentobj.Age = 23;
studentobj.AdmissionDate = new DateTime(2016, 08, 18);
studentobj.IsUG = true;
studentobj.IsPG = "N";*/

////// avoided the new keyword by use of static keyword
/*Student studentobj = new Student.Create("Raghu", "P", 23, new DateTime(2016, 08, 18), true, "N");*/

/*Student studentobj1 = Student.Create(1, "Raghu", "P", 21, new DateTime(2016, 06, 16), true, "N", 1);
Student studentobj2 = Student.Create(2, "Rakesh", "R", 22, new DateTime(2017, 07, 17), true, "N", 2);
Student studentobj3 = Student.Create(3, "Ravi", "S", 23, new DateTime(2018, 08, 18), true, "N", 3);*/

/*List<Student> students = new List<Student>();
students.Add(studentobj1);
students.Add(studentobj2);
students.Add(studentobj3);
*/


/*StudentService studentService = new StudentService();
studentService.Displaystudentdetails(studentobj1);
studentService.Displaystudentdetails(students);*/


StudentService studentService = new StudentService();
DepartmentService departmentService = new DepartmentService();


List<StudentInformation.Coderefactored.Models.Student> students = studentService .GetStudentsdetails();

List<Department> departments = departmentService.GetDepartmentdetails();

var lstofdetails = (from student in students
                    join department in departments
                    on student.DepartmentId equals department.Id
                    select new
                    {
                        StudentID = student.Id,
                        Name = student.FirstName + " , " + student.LastName,
                        student.Age,
                        DepartmentName = department.Name
                    }).ToList();
 
Console.WriteLine("================ List of student details ===============");

foreach(var student in lstofdetails)
{
    Console.WriteLine("====================================");
    Console.WriteLine(student.StudentID);
    Console.WriteLine(student.Name);
    Console.WriteLine(student.Age);
    Console.WriteLine(student.DepartmentName);
    Console.WriteLine("====================================");
}
