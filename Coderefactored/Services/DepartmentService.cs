﻿using StudentInformation.Coderefactored.DataSource;
using StudentInformation.Coderefactored.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInformation.Coderefactored.Sevices
{
    public class DepartmentService
    {
        public void Displaydepartmentdetails(Department department)
        {
            Console.WriteLine(String.Format("Department Id :-> {0} with {1}", department.Id, department.Name));
        }

        public void Displaydepartmentdetails(List<Department> departments)
        {
            foreach (Department department in departments)
            {
                Displaydepartmentdetails(department);
            }
        }

        public List<Department> GetDepartmentdetails()
        {
            return TrialData.GetDepartments();
        }
    }
}
