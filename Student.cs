﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInformation
{

    public class Student
    {

        #region variables

        /*public string fname = string.Empty;
        public string lname = string.Empty;*/

        #endregion variables


        #region property Injection

        /*public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public int Age { get; set; }
        public DateTime AdmissionDate { get; set; }
        public bool IsUG { get; set; }
        public string IsPG { get; set; }
*/
        #endregion property Injection



        #region constructor

        //////empty constuctor
        public Student()
        {

        }


        /////Passing one parameter
        /*public Student(string sfname)
        {
            fname = sfname;
        }*/


        /////Passing two parameters
        /*public Student(string sfname, string slname)
        {
            fname = sfname;
            lname = slname;
        }
*/
        #endregion constructor


        #region method

        /*public void Displayfl()
        {
            Console.WriteLine("Firstname:->" + fname + " Lastname:->" + lname );
        }

        public string Displaystringfl()
        {
            string val = fname + "   " + lname ;
            return val;
        }

        public void Displayfl(string fname, string lname)
        {
            Console.WriteLine("Firstname:->" + fname + " Lastname:->" + lname);
        }

        public string Displaystringfl(string fname, string lname)
        {
            string val = fname + "   " + lname;
            return val;
        }


        public void Displaystudentdetails(Student student)
        {
            Console.WriteLine("Firstname:->" + student.FirstName);
            Console.WriteLine("Lastname:->" + student.LastName);
            Console.WriteLine("Age:->" + student.Age);
            Console.WriteLine("AdmissionDate:->" + student.AdmissionDate.ToString("dddd, dd MMMM yyyy"));
            Console.WriteLine("IsUG:->" + student.IsUG);
            Console.WriteLine("IsPG:->" + student.IsPG);
        }*/

        /*public void Studentanug(Student student)
        {
             if (student.IsUG)
                 Console.WriteLine(" Firstname:-> " + student.FirstName + " Lastname:-> " + student.LastName + " an UG ");
             else
                 Console.WriteLine(" Firstname:-> " + student.FirstName + " Lastname:-> " + student.LastName + " not an UG ");

            bool IsUG = (student.IsUG) && (student.IsPG.Equals("No"));

            if (IsUG)
                Console.WriteLine(" Firstname:-> " + student.FirstName + " Lastname:-> " + student.LastName + " an UG ");
            else
                Console.WriteLine(" Firstname:-> " + student.FirstName + " Lastname:-> " + student.LastName + " an PG ");
        }*/

        /*public void Studentagelimit(Student student)
        {

            if (student.Age < 24)
                Console.WriteLine(" Firstname:->" + student.FirstName + " Lastname:-> " + student.LastName + " an UG ");
            else
                Console.WriteLine(" Firstname:->" + student.FirstName + " Lastname:-> " + student.LastName + " an PG ");
        }*/

       /* public void StudentYearlimitonDate(Student student)
        {

            if (student.AdmissionDate.Year > 2008 && student.AdmissionDate.Year < 2010)
                Console.WriteLine(" Firstname:->" + student.FirstName + " Lastname:-> " + student.LastName + " an UG ");
            else
                Console.WriteLine(" Firstname:->" + student.FirstName + " Lastname:-> " + student.LastName + " an PG ");
        }*/

       /* public void Displaystudentdetails(List<Student> students)
        {
            foreach (Student student in students)
            {
                Displaystudentdetails(student);
            }
        }*/

        /*public void Displaystudentfilterdetails(List<Student> students)
        {
            foreach (Student student in students)
            {
                if (student.Age > 23)

                {
                    Displaystudentdetails(student);
                }

            }*/

            /*List<Student> filterstudents =students.Where(student => student.Age > 23).ToList();
            foreach (Student student in filterstudents)
            {
                Displaystudentdetails(students);
            }*/

            /*students.ForEach(student => { if(student.Age > 23) { Displaystudentdetails(student); } });*/

            /*students.Where(a => a.Age > 23).
                ToList().
                ForEach(a =>
                {
                    Displaystudentdetails(a);
                });*/

            /* students.Where(b => b.FirstName == "Sachin").
                 ToList().
                 ForEach(b => 
                 {
                     Displaystudentdetails(b);
                     });*/

        }
        #endregion method
    }
}

