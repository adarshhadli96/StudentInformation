﻿using StudentInformation.Coderefactored.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentInformation.Coderefactored.Models
{
    public class Student : BaseEntity
    {

        public Student()
        {
            #region variables

            ///default value as empty
            string firstname = string.Empty;
            string lastname = string.Empty;
            int age = 0;
            DateTime admissiondate = DateTime.MinValue;
            bool isug = false;
            string ispg = string.Empty;
        }
            #endregion variables


        #region property

        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public int Age { get; set; }
        public DateTime AdmissionDate { get; set; }
        public bool IsUG { get; set; }
        public string? IsPG { get; set; }
        public int DepartmentId { get; set; }

        #endregion property


        #region method
        public static Student Create(int id, string firstname, string lastname, int age, DateTime admissiondate, bool isug, string ispg, int depid)
        {
            Student student = new Student
            {
                Id = id,
                FirstName = firstname,
                LastName = lastname,
                Age = age,
                AdmissionDate = admissiondate,
                IsUG = isug,
                IsPG = ispg,
                DepartmentId = depid,
            };
            return student;
        }
        #endregion method
    }
}
